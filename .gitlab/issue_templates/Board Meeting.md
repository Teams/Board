# GNOME Foundation Board of Directors Regular Meeting, DATE

## Location

* Call link: https://meet.gnome.org/...
* Access code: CODE

## Attendence

Directors:

* 

Non-directors:

* 

Regrets:

* 

Missing:

* 

##  Call to order

* The chair noted the presence of quorum.
* The chair called the meeting to order at TIME.

## Consideration of the agenda

* The board considered the agenda.

## Consent agenda

* The board approves the minutes from the board meeting held on DATE.
* The board acknowledges receipt of the executive committee meeting minutes held on DATE. 

No objections were made - the consent agenda was passed.

## Discussion item 1

## Discussion item 2

## Executive session opening

* The board entered into executive session at TIME.

`PRIVATE MINUTES BEGIN`

## Executive session consent agenda

* The board approves the minutes from the executive session held on DATE.

## Executive Director's report

## Discussion item 3

`PRIVATE MINUTES END`

## Executive session close

* The board exited executive session at TIME.

## Discussion item 4

## Meeting close

* The chair adjourned the meeting at TIME.

## Appendix (private: do not minute)

* Link to private issue 1
* Link to private issue 2
* Link to private issue 3
