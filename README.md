# Welcome

Welcome to the GNOME Foundation board of Directors. This GitLab project is primarily for internal board use, and is one of the main places where the board does its day to day work. For legal reasons the board cannot make everything it does public, but we do work in the open wherever possible.

The main aspects of this project are the:

 * [Issue tracker](https://gitlab.gnome.org/Teams/Board/-/issues/?sort=closed_at_desc&state=opened&first_page_size=50): for task tracking and discussions
 * [Wiki](https://gitlab.gnome.org/Teams/Board/-/wikis/home): for recording policies and some other public records
 * [Repository](https://gitlab.gnome.org/Teams/Board/-/tree/main): for archiving of meeting minutes and documents
