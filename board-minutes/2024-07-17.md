# Annual Organizing Meeting of the GNOME Foundation Board of Directors, 17 July 2024, 09:00 US/Mountain

## Attendance

### Attending

#### Directors
 * Erik Albers (Chair)
 * Robert McQueen
 * Michael Downey
 * Federico Mena Quintero
 * Allan Day
 * Karen Sandler
 * Julian Sparber
 * Pablo Correa Gomez

#### Non-Directors

 * Rosanna Yuen
 * Richard Littauer
 * Jeremy Allison

### Regrets

 * Sammy Fung
 * Holly Million

### Missing

 * Regina Nkenchor

### Guests & Observers

 * None

## Meeting Minutes

### Call to Order

 * The Chair suggests scheduling a break to improve AV setup in Berlin.
 * **The Chair called the meeting to order at 09:07 US/Mountain.**
 * The Chair noted the presence of a quorum (8), being a majority of the current authorized number of Directors.

### Consideration of the agenda

 * The Chair suggested the proposed agenda as listed in issue [#691](https://gitlab.gnome.org/Teams/Board/-/issues/691)

## New Business 

 * The Directors and other participants took a round to introduce themselves and their wishes for the Board in the forthcoming year.
 * Transition tasks in [#682](https://gitlab.gnome.org/Teams/Board/-/issues/682) have been largely completed; Rob will ensure the final items are closed out.
 * Directors were reminded to join BoardSource and complete the training as outlined in [#701](https://gitlab.gnome.org/Teams/Board/-/issues/701).
 * Selection of officers [#693](https://gitlab.gnome.org/Teams/Board/-/issues/693)
   * President (willing: Rob, Julian)
     * **Rob and Julian temporarily left the meeting during discussion of and vote for this role's selection.**
     * The remaining Board members deliberated the candidates for this officer role.
     * **VOTE 2024-001:** The Board voted unanimously to appoint Rob as President.
     * **Rob and Julian rejoined the meeting.**
    * Secretary (willing: Julian, Federico)
        * Possible "executive secretary" staff role?
        * **Julian and Federico temporarily left the meeting during discussion of and vote for this role's selection.**
        * **VOTE 2024-002:** The Board voted unanimously to appoint Julian as Secretary.
        * **Julian and Federico rejoined the meeting.**
    * Treasurer (willing: Pablo, Michael)
        * Ensures board oversight of the Foundation's financials
        * Regularly reports to the board on the Foundation's finances (cash, receivables, outstanding bills, expenditures, income)
        * Sits on the compensation committee
        * Sits on the finance committee 
        * **Pablo and Michael temporarily left the meeting during discussion of and vote for this role's selection.**
        * **VOTE 2024-003:** The Board voted 3:2 (1 abstain) to appoint Michael as Treasurer.
        * **Pablo and Michael rejoined the meeting.**
    * Vice-president (willing: Allan)
        * Stands in for the President in their absence 
    * Chair (optional; willing: Allan)
        * Prepares an agenda and sends it to the board before each meeting
        * Chairs each board meeting
        * Sits on the governance committee 
    * Assistants (optional, TBD)
        * Vice-secretary: Federico
        * Vice-treasurer: Pablo
    * **VOTE 2024-004**: Pursuant to Section 10.2 of the Bylaws, the Board unanimously elects the remaining officer appointments per slate above for Vice-President, Chair, and Assistants.

* Board committees
    * Exec Committee
        * Rob (President, ex officio)
        * Richard (ED, ex officio, non-voting)
        * Julian (Secretary)
        * Allan (Chair, VP)
        * Rosanna (Senior Director of People, ex-officio, non-voting)
    * Governance Committee
        * Allan (Chair, ex officio)
        * Karen
        * Erik
        * Federico
    * Finance Committee
        * Michael (Treasurer, ex officio)
        * Richard (ED, ex officio)
        * Rosanna (Senior Director of People, ex officio)
        * Pablo (Vice-treasurer)
    * **VOTE 2024-005:** Pursuant to Section 9.1 of the Bylaws, the Board unanimously voted to appoint the above slate of committee members.

 * **The Board recessed at 12:45 US/Mountain for a 1-hour meal break.**
 * **The Board reconvened at 13:52 US/Mountain, with a quorum again present. Former Director Allison did not return for the afternoon session.**
 * The Board reviewed the outstanding agenda items for priority.
 * **Karen temporarily departed the meeting at 14:00 US/Mountain.**
 * The Board discussed expanding its capacity and collaborative communication for timely action: 
   * Why can't we publish minutes immediately?  Because they need to be voted on - during the next meeting.  We make the first point of each meeting to approve last meeting's minutes for publication.
     * Discussions included increased division of labor via working groups or similar bodies that evaluate issues in detail and then would report back recommendations to the board.
     * One possibility for improvement is a cleaner way to handoff incoming issues (via GitLab) to staff and monitor its completion status.
       * GitLab issues are tagged as "delegated to staff" and closed, but it's hard to follow up after that.
     * The GitLab configuration needs improvement to more clearly separate inbound "external" board inquiries and internal deliberation/discussion.
       * Sending mail to board@ alias creates a GitLab ticket.  It's not clear whether the ticket is the proper forum to reply to the original mail.  Also, GitLab notifications when someone checks/unchecks a checkbox get sent to the original sender, which makes such issues inappropriate for internal discussion.
     * Anything that goes in GitLab can be summoned during a lawsuit, so it will be part of a case, possibly taken out of context, etc.
     * Have an index of votes or motions, in addition to the minutes: an index of key decisions.
     * **ACTION:** The Governance Committee is tasked to investigate alternative processes for more timely approval and publication of minutes that is also consistent with applicable law. [#702](https://gitlab.gnome.org/Teams/Board/-/issues/702)

* **The Chair recessed the meeting until tomorrow, Thursday July 18, at 08:00 US/Mountain.** 

