# GNOME Foundation Board of Directors Special Meeting, 22 October 2024

## Attendence

Directors:

* Allan Day
* Erik Albers (+:10)
* Federico Mena Quintero
* Julian Sparber
* Karen Sandler
* Michael Downey (+:06)
* Pablo Correa Gomez
* Robert McQueen

Non-directors:

* Richard Littauer

##  Call to order

* The chair noted the presence of quorum.
* The chair called the meeting to order at 16:05 UTC

## Executive session opening

* The board entered into executive session at 16:07.

## Executive session closing

* The board exited executive session at 17:47 UTC`.

## Meeting close

* The chair adjourned the meeting at 17:47 UTC.