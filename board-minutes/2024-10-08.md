# Regular board Meeting, 08 October 2024

## Attendance

### Attending

#### Directors

 * Julian Sparber
 * Allan Day (Chair)
 * Federico Mena Quintero
 * Michael Downey
 * Pablo Correa Gomez
 * Erik Albers
 * Robert McQueen
 * Karen Sandler (+ :12)

#### Non-Directors

### Regrets

 * Richard Littauer

### Missing

## Meeting minutes

## Opening

* Call to order 17:08 UTC 
* The chair confirms the board meeting has quorum.

## Consent agenda

* The board approves the minutes from 10 and 24 September 2024.
* The board acknowledges receipt of the executive committee minutes from: 12, 19, 26 September 2024.

## Discussion agenda

* Advisory board meeting at FOSDEM 2025

    * Traditionally we had a AB meeting at FOSDEM
        * Problem now is travel sponsorhip with the reduced budget
        * Who can go from the board on their own expense? 
            * Karen, Pablo, Erik will be at FOSDM
            * Rob can go if we're doing stuff
            * Federico, Michael would need work support
            * Allan still considering it
        * Previous years we booked a room/location for the meeting and a board hackfest
            * ACTION: Task staff (ask richard) to find a venue Erik will follow up

* ED hiring: #734
    * Julian has reached out to some individuals discussed in the EC to appoint an ED Search Committee.
    * Is it an official committee?
        * It's been informal previously, and has made a recommendation for the board's final decision.
        * Maybe better to call it something else if not official?
    * Is the scope of the committee to make a single recommendation or a shortlist?
        * Both have happened in the past; the committee has suggested a couple of candidates and the board has made additional interviewing, and on another occasion the committee has only been aligned around one candidate.
        * A number of directors express a preference that the board has a slate of candidates to choose from.
    * General consensus to formally appoint a committee and ask them to screen and interview candidates and present a shortlist.
    * The proposed members are: 
        * Deb Nicholson
        * Jonathan Blandford
        * Julian Sparber
        * Julian Hofer
        * Regina Nkenchor (pending acceptance)
        * Rob McQueen
        * Rosanna Yuen (administrative role)
    * VOTE-2024-xxx: Appoint the committee named above to screen and interview ED applicants and present a shortlist to the board for final review and appointment.
        * Passed unanimously
* The board entered Executive session (private minutes)
* Executive session closed at 17:57 UTC

* Quick consultation:
   * Events strategy (#713)
      * Is the board in favor of us reviewing our events strategy in light of the reactions we got from GUADEC 2024?
          * Do you agree that we should review our event stratagy in the next months, because there will be a call for the next GUADEC soon so we need to figure this out soon if we want a change.
          * ACTION: Please comment on #713 if this should happen and if yes, with your views on what the strategy should look like, and how we should have the conversation
   * Conflict between election and budgeting cycles #752
      * ACTION: Please review the ticket and comment as to whether you think we should a) not change anything b) change the financial year c) move the election dates

The chair closed the meeting at 18:03 UTC